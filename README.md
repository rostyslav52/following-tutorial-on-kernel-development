# Abandoned

I used this repo to follow the 2nd edition of Philipp Oppermann's tutorial (https://os.phil-opp.com).

Unfortunately, by the time I reached topics of software and hardware interrupts, I understood that this tutorial hides too many important details. 

It is perfect for learning theory (Philipp is amazing at presenting important info in a concise manner), but it is less than perfect in regards to practice : half of the code is hidden behind libraries (crates) and the rest is too dependant on those librairies. As a result, I ended up copy-pasting too much code.

Even though I understood all the code I wrote, I am afraid that I won't retain information in my head for long. That is why the best approach is to start a new project of writing a toy kernel. I finished reading the Philipp's tutorial, but decided that copy-pasting of code is useless.

With the experience from Philipp's tutorial (notably, Rust's lazy statics) I feel ready to start implementing a toy kernel by myself in a separate git repository. What is more, it will be an opportunity to see architectures other than x86_64.

For information, to check out what is already here, execute `cargo run` and `cargo test` for testing.
