#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(following_kernel_tutorial::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use following_kernel_tutorial::print;
use following_kernel_tutorial::println;
use following_kernel_tutorial::serial_println;

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}

//TODO : is it a duplicate?
#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    following_kernel_tutorial::test_panic_handler(info)
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    serial_println!("_start() is now executing");
    println!("Hello World{}", "!");
    following_kernel_tutorial::init();
    x86_64::instructions::interrupts::int3();
    println!("And now... New line :");
    println!("And now I am going to write some really long text that is supposed to go beyond 80 characters to test whether it wraps or not...");
    println!("Apparently, it does :)");
    /*
    println!("");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("New line");
    println!("Testing formatting macro : {} and {}", 42, 1.0 / 3.0);
    println!("New line");
    println!("New line");
    println!("First line disappeared as expected due to scrolling :)");
    print!("Checking that cursor is moved... ");
    */
    #[cfg(test)]
    test_main();

    loop {}
}
