use core::fmt;
use lazy_static::lazy_static;
use spin::Mutex;
use x86_64::instructions::port::Port;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(dead_code)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct FormattingByte(u8);

impl FormattingByte {
    fn new(text_color: Color, background_color: Color) -> FormattingByte {
        return FormattingByte(((background_color as u8) << 4) | (text_color as u8));
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)] //we want for 2 attributes to be stored contiguously and always in the same order
struct ScreenCharacter {
    ascii_character: u8,
    formatting_code: FormattingByte,
}

const VGA_BUFFER_ADDRESS: *mut Buffer = 0xb8000 as *mut Buffer;
const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

#[repr(transparent)]
struct Buffer {
    content: [[ScreenCharacter; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    cursor_position_x: usize,
    cursor_position_y: usize,
    formatting: FormattingByte,
    buffer_reference: &'static mut Buffer,
}

lazy_static! {
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer::new());
}

impl Writer {
    pub fn new() -> Writer {
        return Writer {
            cursor_position_x: 0,
            cursor_position_y: 0,
            formatting: FormattingByte::new(Color::Magenta, Color::Black),
            buffer_reference: unsafe { &mut *VGA_BUFFER_ADDRESS },
        };
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.cursor_position_x >= BUFFER_WIDTH {
                    self.new_line();
                }

                self.buffer_reference.content[self.cursor_position_y][self.cursor_position_x] =
                    ScreenCharacter {
                        ascii_character: byte,
                        formatting_code: self.formatting,
                    };
                self.cursor_position_x += 1;
                self.update_cursor();
                //TODO : check performance. Maybe make write_byte private and move update_cursor() to write_string?
            }
        }
    }

    pub fn write_string(&mut self, string: &str) {
        for byte in string.bytes() {
            match byte {
                // printable ASCII byte or newline
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                // not part of printable ASCII range
                _ => self.write_byte(0xfe),
            }
        }
    }

    pub fn new_line(&mut self) {
        self.cursor_position_x = 0;
        self.cursor_position_y += 1;
        if self.cursor_position_y >= BUFFER_HEIGHT - 1 {
            self.scroll_screen_up();
            self.cursor_position_y -= 1;
        }
        self.update_cursor();
    }

    pub fn scroll_screen_up(&mut self) {
        for y in 0..(BUFFER_HEIGHT - 1) {
            for x in 0..BUFFER_WIDTH {
                self.buffer_reference.content[y][x] = self.buffer_reference.content[y + 1][x];
            }
        }
    }

    fn update_cursor(&self) {
        let position: usize = self.cursor_position_x + self.cursor_position_y * BUFFER_WIDTH;
        unsafe {
            let mut port1 = Port::new(0x3D4);
            let mut port2 = Port::new(0x3D5);
            port1.write(0x0F as u8);
            port2.write((position & 0xFF) as u8);
            port1.write(0x0E as u8);
            port2.write(((position >> 8) & 0xFF) as u8);
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, string: &str) -> fmt::Result {
        self.write_string(string);
        return Ok(());
    }
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}

// Tests ----------------------------
#[test_case]
fn test_println_no_panic() {
    println!("test_println_simple output");
}

#[test_case]
fn test_println_many() {
    for _ in 0..200 {
        println!("test_println_many output");
    }
}

#[test_case]
fn test_println_output() {
    let s = "Some test string that fits on a single line";
    println!("{}", s);
    let cursor_line = WRITER.lock().cursor_position_y;
    for (i, c) in s.chars().enumerate() {
        let screen_char = WRITER.lock().buffer_reference.content[cursor_line - 1][i];
        assert_eq!(char::from(screen_char.ascii_character), c);
    }
}
